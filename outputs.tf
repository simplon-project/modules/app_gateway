output "app_gateway_name" {
  description = "Le nom de la passerelle d'application"
  value       = azurerm_application_gateway.appgw.name
}

output "app_gateway_resource_group_name" {
  description = "Le nom du groupe de ressources de la passerelle d'application"
  value       = azurerm_application_gateway.appgw.resource_group_name
}

output "app_gateway_location" {
  description = "L'emplacement de la passerelle d'application"
  value       = azurerm_application_gateway.appgw.location
}

output "app_gateway_sku_name" {
  description = "Le nom du SKU de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.sku)[0].name
}

output "app_gateway_sku_tier" {
  description = "Le niveau du SKU de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.sku)[0].tier
}

output "app_gateway_sku_capacity" {
  description = "La capacité du SKU de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.sku)[0].capacity
}

output "app_gateway_gateway_ip_configuration_name" {
  description = "Le nom de la configuration IP de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.gateway_ip_configuration)[0].name
}

output "app_gateway_gateway_ip_configuration_subnet_id" {
  description = "L'ID du sous-réseau de la configuration IP de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.gateway_ip_configuration)[0].subnet_id
}

output "app_gateway_frontend_port_name" {
  description = "Le nom du port frontal de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.frontend_port)[0].name
}

output "app_gateway_frontend_port_port" {
  description = "Le port frontal de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.frontend_port)[0].port
}

output "app_gateway_frontend_ip_configuration_name" {
  description = "Le nom de la configuration IP frontale de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.frontend_ip_configuration)[0].name
}

output "app_gateway_backend_address_pool_name" {
  description = "Le nom du pool d'adresses de backend de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.backend_address_pool)[0].name
}

output "app_gateway_backend_http_settings_name" {
  description = "Le nom des paramètres HTTP de backend de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.backend_http_settings)[0].name
}

output "app_gateway_backend_http_settings_cookie_based_affinity" {
  description = "L'affinité basée sur les cookies des paramètres HTTP de backend de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.backend_http_settings)[0].cookie_based_affinity
}

output "app_gateway_backend_http_settings_port" {
  description = "Le port des paramètres HTTP de backend de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.backend_http_settings)[0].port
}

output "app_gateway_backend_http_settings_protocol" {
  description = "Le protocole des paramètres HTTP de backend de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.backend_http_settings)[0].protocol
}

output "app_gateway_backend_http_settings_request_timeout" {
  description = "Le délai d'attente des paramètres HTTP de backend de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.backend_http_settings)[0].request_timeout
}

output "app_gateway_probe_name" {
  description = "Le nom de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].name
}

output "app_gateway_probe_protocol" {
  description = "Le protocole de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].protocol
}

output "app_gateway_probe_host" {
  description = "L'hôte de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].host
}

output "app_gateway_probe_path" {
  description = "Le chemin de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].path
}

output "app_gateway_probe_timeout" {
  description = "Le délai d'attente de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].timeout
}

output "app_gateway_probe_interval" {
  description = "L'intervalle de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].interval
}

output "app_gateway_probe_unhealthy_threshold" {
  description = "Le seuil de non-santé de la sonde de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.probe)[0].unhealthy_threshold
}

output "app_gateway_http_listener_name" {
  description = "Le nom de l'écouteur HTTP de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.http_listener)[0].name
}

output "app_gateway_http_listener_frontend_ip_configuration_name" {
  description = "Le nom de la configuration IP frontale de l'écouteur HTTP de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.http_listener)[0].frontend_ip_configuration_name
}

output "app_gateway_http_listener_frontend_port_name" {
  description = "Le nom du port frontal de l'écouteur HTTP de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.http_listener)[0].frontend_port_name
}

output "app_gateway_http_listener_protocol" {
  description = "Le protocole de l'écouteur HTTP de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.http_listener)[0].protocol
}

output "app_gateway_redirect_configuration_name" {
  description = "Le nom de la configuration de redirection de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.redirect_configuration)[0].name
}

output "app_gateway_redirect_type" {
  description = "Le type de redirection de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.redirect_configuration)[0].redirect_type
}

output "app_gateway_redirect_target_listener_name" {
  description = "Le nom de l'écouteur cible de la redirection de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.redirect_configuration)[0].target_listener_name
}

output "app_gateway_redirect_include_path" {
  description = "Inclure le chemin dans la redirection de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.redirect_configuration)[0].include_path
}

output "app_gateway_redirect_include_query_string" {
  description = "Inclure la chaîne de requête dans la redirection de la passerelle d'application"
  value       = tolist(azurerm_application_gateway.appgw.redirect_configuration)[0].include_query_string
}


# propriétés de la passerelle d'application v2
output "app_gateway_id" {
  description = "L'ID de la passerelle d'application"
  value       = azurerm_application_gateway.appgw.id
}
output "app_gateway_dns_name" {
  description = "Le nom DNS de la passerelle d'application"
  value       = azurerm_public_ip.gw-ippu.fqdn
}
output "app_gateway_url" {
  description = "L'IP publique de la passerelle d'application"
  value       = azurerm_public_ip.gw-ippu.ip_address
}


output "public_ip_address" {
  description = "The public IP address of the Azure Public IP resource."
  value       = azurerm_public_ip.gw-ippu.ip_address
}

output "public_ip_dns" {
  description = "The DNS name of the Azure Public IP resource."
  value       = azurerm_public_ip.gw-ippu.domain_name_label
}

output "public_ip_id" {
  description = "The ID of the Azure Public IP resource."
  value       = azurerm_public_ip.gw-ippu.id
}
