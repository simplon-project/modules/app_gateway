variable "resource_group_name" {
  description = "Resource Group Name"
  type        = string
}

variable "subnet_id" {
  description = "Subnet ID for the Application Gateway"
  type        = string
}

variable "ip_public_app_gw" {
  description = "Public IP for the Application Gateway"

  type = object({
    name                = string
    resource_group_name = string
    location            = string
    sku                 = string
    domain_name_label   = string
    allocation_method   = string
  })
  default = {
    name                = "app-gw-ip-pub"
    resource_group_name = null
    location            = "westeurope"
    sku                 = "Standard"
    domain_name_label   = "dns-app-gw"
    allocation_method   = "Static"
  }
}

variable "app_gw_properties" {
  description = "Les propriétés de la passerelle d'application"
  type = list(object({
    gw_name                        = string
    location                       = string
    resource_group_name            = string
    domain_name_label              = string
    allocation_method              = string
    sku                            = string
    sku_name                       = string
    sku_tier                       = string
    sku_capacity                   = number
    # subnet_id                      = string
    frontend_port_name             = string
    frontend_port                  = number
    frontend_ip_configuration_name = string
    backend_address_pool_name      = string
    backend_http_settings_name     = string
    cookie_based_affinity          = string
    backend_http_settings_port     = number
    backend_http_settings_protocol = string
    request_timeout                = number
    probe_name                     = string
    probe_protocol                 = string
    probe_host                     = string
    probe_path                     = string
    probe_timeout                  = number
    probe_interval                 = number
    unhealthy_threshold            = number
    http_listener_name             = string
    http_listener_protocol         = string
    redirect_configuration_name    = string
    redirect_type                  = string
    target_listener_name           = string
    include_path                   = bool
    include_query_string           = bool
    request_routing_rule_name      = string
    rule_type                      = string
    log_analytics_workspace_name   = string
    ssl_profile = list(object({
      name                                 = string
      trusted_client_certificate_names     = list(string)
      verify_client_cert_issuer_dn         = bool
      verify_client_certificate_revocation = string
    }))
    ssl_policy = list(object({
      # disabled_protocols   = list(string)
      policy_type          = string
      policy_name          = string
      cipher_suites        = list(string)
      min_protocol_version = string
    }))
    trusted_root_certificate = list(object({
      name = string
      data = string
    }))
    enable_http2 = bool
  }))
  default = [{
    gw_name                        = "appgw-name"
    location                       = "westeurope"
    resource_group_name            = null
    domain_name_label              = "dns-app-gw"
    allocation_method              = "Static"
    sku                            = "Standard_v2"
    sku_name                       = "Standard_v2"
    sku_tier                       = "Standard_v2"
    sku_capacity                   = 2
    application_gateway_subnet_id  = "/subscriptions/c56aea2c-50de-4adc-9673-6a8008892c21/resourceGroups/Gregory_M_Projet_Final/providers/Microsoft.Network/virtualNetworks/vnet1/subnets/snet_app_gw"
    frontend_port_name             = "port-80"
    frontend_port                  = 80
    frontend_ip_configuration_name = "appGatewayFrontendIP"
    backend_address_pool_name      = "pool-backend"
    backend_http_settings_name     = "http-settings"
    cookie_based_affinity          = "Disabled"
    backend_http_settings_port     = 80
    backend_http_settings_protocol = "Http"
    request_timeout                = 20
    probe_name                     = "probe"
    probe_protocol                 = "Http"
    probe_host                     = "localhost"
    probe_path                     = "/"
    probe_timeout                  = 20
    probe_interval                 = 30
    unhealthy_threshold            = 3
    http_listener_name             = "http-listener"
    http_listener_protocol         = "Http"
    redirect_configuration_name    = "redirectConfiguration"
    redirect_type                  = "Permanent"
    target_listener_name           = "http-listener"
    include_path                   = true
    include_query_string           = true
    request_routing_rule_name      = "rule"
    rule_type                      = "Basic"
    log_analytics_workspace_name   = null
    ssl_profile = [
      /*{
        name                                 = "mySslProfile"
        trusted_client_certificate_names     = ["self_cert"]
        verify_client_cert_issuer_dn         = true
        verify_client_certificate_revocation = "OCSP"
      }*/
    ]
    ssl_policy = [
      {
        # disabled_protocols   = ["TLSv1_0"]
        policy_type          = "Predefined"
        policy_name          = "AppGwSslPolicy20240621"
        cipher_suites        = ["TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"]
        min_protocol_version = "TLSv1_2"
      }
    ]
    trusted_root_certificate = [
      /*{
        name = "myTrustedRootCertificate"
        data = filebase64("path/to/certificate")
      }*/
    ]
    enable_http2 = true
  }]
}