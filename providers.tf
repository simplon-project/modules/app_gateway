terraform {
  required_version = ">= 1.8.3"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.106.1"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "c56aea2c-50de-4adc-9673-6a8008892c21"
  # tenant_id       = "16763265-1998-4c96-826e-c04162b1e041"
}
