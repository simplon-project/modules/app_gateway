data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

resource "azurerm_public_ip" "gw-ippu" {
  name                = var.ip_public_app_gw.name
  location            = var.ip_public_app_gw.location
  resource_group_name = data.azurerm_resource_group.rg.name
  domain_name_label   = var.ip_public_app_gw.domain_name_label
  allocation_method   = var.ip_public_app_gw.allocation_method
  sku                 = var.ip_public_app_gw.sku
}

resource "azurerm_application_gateway" "appgw" {
  name                = var.app_gw_properties[0].gw_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.app_gw_properties[0].location

  sku {
    name     = var.app_gw_properties[0].sku_name
    tier     = var.app_gw_properties[0].sku_tier
    capacity = var.app_gw_properties[0].sku_capacity
  }

  gateway_ip_configuration {
    name      = var.app_gw_properties[0].frontend_ip_configuration_name
    subnet_id = var.subnet_id
  }
  frontend_port {
    name = var.app_gw_properties[0].frontend_port_name
    port = var.app_gw_properties[0].frontend_port
  }

  frontend_ip_configuration {
    name                 = "appGatewayFrontendIP"
    public_ip_address_id = azurerm_public_ip.gw-ippu.id
  }

  backend_address_pool {
    name = var.app_gw_properties[0].backend_address_pool_name
  }

  backend_http_settings {
    name                  = var.app_gw_properties[0].backend_http_settings_name
    cookie_based_affinity = var.app_gw_properties[0].cookie_based_affinity
    port                  = var.app_gw_properties[0].backend_http_settings_port
    protocol              = var.app_gw_properties[0].backend_http_settings_protocol
    request_timeout       = var.app_gw_properties[0].request_timeout
    probe_name            = var.app_gw_properties[0].probe_name
  }

  probe {
    name                = var.app_gw_properties[0].probe_name
    protocol            = var.app_gw_properties[0].probe_protocol
    host                = var.app_gw_properties[0].probe_host
    path                = var.app_gw_properties[0].probe_path
    timeout             = var.app_gw_properties[0].probe_timeout
    interval            = var.app_gw_properties[0].probe_interval
    unhealthy_threshold = var.app_gw_properties[0].unhealthy_threshold
  }

  http_listener {
    name                           = var.app_gw_properties[0].http_listener_name
    frontend_ip_configuration_name = "appGatewayFrontendIP"
    frontend_port_name             = var.app_gw_properties[0].frontend_port_name
    protocol                       = var.app_gw_properties[0].http_listener_protocol
  }

  redirect_configuration {
    name                 = var.app_gw_properties[0].redirect_configuration_name
    redirect_type        = var.app_gw_properties[0].redirect_type
    target_listener_name = var.app_gw_properties[0].target_listener_name
    include_path         = var.app_gw_properties[0].include_path
    include_query_string = var.app_gw_properties[0].include_query_string
  }

  request_routing_rule {
    name                       = var.app_gw_properties[0].request_routing_rule_name
    rule_type                  = var.app_gw_properties[0].rule_type
    http_listener_name         = var.app_gw_properties[0].http_listener_name
    backend_address_pool_name  = var.app_gw_properties[0].backend_address_pool_name
    backend_http_settings_name = var.app_gw_properties[0].backend_http_settings_name
    priority                   = 100
  }
  # ssl_certificate {
  #   name     = "self_cert"
  #   data     = filebase64("${path.root}/certificate.pem")
  #   password = var.pfx_password
  # }
  dynamic "ssl_profile" {
    for_each = var.app_gw_properties[0].ssl_profile
    content {
      name                                 = ssl_profile.value.name
      trusted_client_certificate_names     = ssl_profile.value.trusted_client_certificate_names
      verify_client_cert_issuer_dn         = ssl_profile.value.verify_client_cert_issuer_dn
      verify_client_certificate_revocation = ssl_profile.value.verify_client_certificate_revocation
    }
  }

  dynamic "ssl_policy" {
    for_each = var.app_gw_properties[0].ssl_policy
    content {
      # disabled_protocols   = ssl_policy.value.disabled_protocols
      policy_type          = ssl_policy.value.policy_type
      policy_name          = ssl_policy.value.policy_name
      cipher_suites        = ssl_policy.value.cipher_suites
      min_protocol_version = ssl_policy.value.min_protocol_version
    }
  }

  dynamic "trusted_root_certificate" {
    for_each = var.app_gw_properties[0].trusted_root_certificate
    content {
      name = trusted_root_certificate.value.name
      data = trusted_root_certificate.value.data
    }
  }

  waf_configuration {
    enabled          = true
    firewall_mode    = "Detection"
    rule_set_type    = "OWASP"
    rule_set_version = "3.0"
  }

  enable_http2 = var.app_gw_properties[0].enable_http2

  lifecycle {
    ignore_changes = [
      request_routing_rule,
      http_listener,
      frontend_port,
      backend_http_settings,
      backend_address_pool,
      ssl_certificate,
      probe,
      redirect_configuration,
      url_path_map,
      trusted_root_certificate,
      ssl_policy,
      ssl_profile,
      tags
    ]
  }
}


